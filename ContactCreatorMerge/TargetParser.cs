﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;
using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MergeContactCreator
{
    public class TargetParser
    {
        protected ContactCreatorRow[] ContactCreatorRows { get; set; }

        protected string[] HeaderNames { get; private set; }

        protected string AddressHeaderName { get; private set; }

        public string TargetFile => ConfigurationManager.AppSettings["TargetFileName"];

        public string Suffix => ConfigurationManager.AppSettings["NewFileSuffix"];

        public void OpenExportedData(string dataFile)
        {
            using (var fileStream = File.Open(dataFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var textReader = new StreamReader(fileStream))
                {
                    using (var csv = new CsvReader(textReader))
                    {
                        ContactCreatorRows = csv.GetRecords<ContactCreatorRow>().ToArray();
                    }
                }
            }

        }

        public void SetHeaderName(string headerName)
        {
            AddressHeaderName = HeaderNames?.Where(obj => obj == headerName)?.SingleOrDefault();
            if(AddressHeaderName==null)
                throw new ArgumentException("No header name found");
        }

        //public void AddRadioButtons(ContainerControl parent)
        //{
        //    parent.Controls.Clear();
        //    for (var pos = HeaderNames.GetLowerBound(0); pos <= HeaderNames.GetUpperBound(0); pos++)
        //    {
        //        var button = new Button();
        //        button.Text = HeaderNames[pos];
        //        button.Click += Button_Click;
        //        parent.Controls.Add(button);
        //        button.Top = pos * button.Height + 10;
        //        button.Left = 5;
        //    }   
        //}

        //private void Button_Click(object? sender, EventArgs e)
        //{
        //    var btn = sender as Button;
        //    var parent = btn.Parent;
        //    foreach (Control ctrl in parent.Controls)
        //    {
        //        ctrl.Enabled = false;
        //    }
        //    var val = btn.Text;
        //    ParseInitialData(TargetFile, val);
        //    parent.Controls.Clear();
        //    while (parent.Parent != null)
        //        parent = parent.Parent;
        //    (parent as Form).Close();

        //}

        public void GetHeaderNames()
        {
            using (var fileStream = File.Open(TargetFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var textReader = new StreamReader(fileStream))
                {
                    using (var csv = new CsvReader(textReader))
                    {
                        if (csv.Read())
                        {
                            var item = csv.GetRecord<dynamic>();
                            var itemAsColl = item as IDictionary<string, object>;
                            HeaderNames = itemAsColl.Keys.ToArray();
                        }
                    }
                }
            }
        }

        public void ParseInitialData(string filePath)
        {
            var returnItems = new List<dynamic>();
            using (var fileStream = File.Open(TargetFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var textReader = new StreamReader(fileStream))
                {
                    using (var csv = new CsvReader(textReader))
                    {
                        while (csv.Read())
                        {
                            var item = csv.GetRecord<dynamic>();
                            var itemAsColl = item as IDictionary<string, object>;
                            var fullAddress = itemAsColl[AddressHeaderName] as string;
                            var arr = fullAddress.Split(',').Select(obj => obj.Trim().ToUpperInvariant()).ToArray();
                            var postcode = arr.Last().Replace(" ", string.Empty).ToUpperInvariant();
                            var rows = ContactCreatorRows
                                .Where(obj =>
                                    obj.Postcode.Trim().ToUpperInvariant().Replace(" ", string.Empty) == postcode)
                                .Where(obj =>
                                    fullAddress.ToUpperInvariant()
                                        .Contains(obj.AddressLine1.Replace("Flat", string.Empty).Trim().ToUpperInvariant()));
                            foreach (var row in rows)
                            {
                                var newItem =
                                    (dynamic)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(item));
                                newItem.FulllName = row.FullName;
                                newItem.Title = row.Title;
                                newItem.Forename = row.Forename;
                                newItem.Surname = row.Surname;
                                newItem.AddressLine1 = row.AddressLine1;
                                newItem.AddressLine2 = row.AddressLine2;
                                newItem.AddressLine3 = row.AddressLine3;
                                newItem.AddressLine4 = row.AddressLine4;
                                newItem.Postcode = row.Postcode;
                                newItem.Constituency = row.Constituency;
                                newItem.Ward = row.Ward;
                                newItem.PollingDistrict = row.PollingDistrict;
                                newItem.RoadGroup = row.RoadGroup;
                                newItem.IsPostal = row.IsPostal;
                                newItem.PropertyTelephoneNumber = row.PropertyTelephoneNumber;
                                newItem.PersonTelephoneNumber = row.PersonTelephoneNumber;
                                newItem.Email = row.Email;
                                newItem.MosaicGroup = row.MosaicGroup;
                                newItem.MosaicType = row.MosaicType;
                                newItem.VoterIdHistory = row.VoterIdHistory;
                                returnItems.Add(newItem);
                            }
                        }
                    }

                }
            }

            var lastIndexOfdot = filePath.LastIndexOf('.');
            var firstBit = filePath.Substring(0, lastIndexOfdot);
            var lastBit = filePath.Substring(lastIndexOfdot);
            var newFileName = string.Concat(firstBit, Suffix, lastBit);

            using(var fileStream = File.Open(newFileName, FileMode.CreateNew, FileAccess.Write, FileShare.Read))
            {
                using (var textWriter = new StreamWriter(fileStream, Encoding.UTF8))
                {
                    using (var csv = new CsvWriter(textWriter))
                    {
                        var firstItem = (JObject) returnItems.FirstOrDefault();
                        if (firstItem == null)
                            return;
                        foreach (var item in firstItem.Properties())
                        {
                            csv.WriteField(item.Name);
                        }
                        foreach (var row in returnItems)
                        {
                            csv.NextRecord();
                            var dictionaryItem = (JObject)row;
                            foreach (var item in dictionaryItem.Properties())
                            {
                                csv.WriteField(dictionaryItem.GetValue(item.Name).ToString());
                            }
                        }
                    }
                }
            }
        }
    }
}
