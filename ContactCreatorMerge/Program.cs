﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeContactCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileName = args?.FirstOrDefault();
            if(fileName==null)
                throw new ArgumentNullException(nameof(args));
            var fi = new FileInfo(fileName);
            if(!fi.Exists)
                throw new ApplicationException("No such file");
            var di = fi.Directory;
            var parser = new TargetParser();
            parser.OpenExportedData(fi.FullName);
            parser.GetHeaderNames();
            parser.SetHeaderName("Full Address");
            parser.ParseInitialData(fi.FullName);
        }
    }
}
