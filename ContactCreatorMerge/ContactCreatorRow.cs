﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;

namespace MergeContactCreator
{
    public class ContactCreatorRow
    {
        [Name("unique person code")] public long PersonCode { get; set; }
        [Name("constituency")] public string Constituency { get; set; }
        [Name("ward")] public string Ward { get; set; }
        [Name("polling district")] public string PollingDistrict { get; set; }
        [Name("title")] public string Title { get; set; }
        [Name("forename (actual)")] public string Forename { get; set; }
        [Name("surname (actual)")] public string Surname { get; set; }
        [Name("full name")] public string FullName { get; set; }
        [Name("property telephone number")] public string PropertyTelephoneNumber { get; set; }
        [Name("person telephone number")] public string PersonTelephoneNumber { get; set; }
        [Name("person email")] public string Email { get; set; }
        [Name("property address 1 (actual)")] public string AddressLine1 { get; set; }
        [Name("property address 2")] public string AddressLine2 { get; set; }
        [Name("property address 3")] public string AddressLine3 { get; set; }
        [Name("property address 4")] public string AddressLine4 { get; set; }
        [Name("town")] public string Town { get; set; } 
        [Name("postcode")] public string Postcode { get; set; }
        [Name("road group")] public string RoadGroup { get; set; }
        [Name("MOSAIC group")] public string MosaicGroup { get; set; }
        [Name("MOSAIC type")] public string MosaicType { get; set; }
        [Name("current absent voter")] public string CurrentAbsentVoter { get; set; }
        [Name("IsPostal")] public bool IsPostal => CurrentAbsentVoter?.ToLowerInvariant()?.Trim() == "postal";
        [Name("voter id history")] public string VoterIdHistory { get; set; }

    }
}
